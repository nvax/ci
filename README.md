# Gitlab CI Base

Some base GitlabCI Configuration-Files.

## Configuration

### [GitlabCI: Stages](config/ci.stages.yaml)

### [Docker: Base Job](config/docker.base.yaml)

### [Docker: Build & Publish](config/docker.publish.yaml)

### [Environment Variables: Versions](config/env.versions.yaml)

### [Git: Tag Commit](config/git.tag.yaml)
